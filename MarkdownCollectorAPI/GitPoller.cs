using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using LibGit2Sharp;

namespace MarkdownCollectorAPI {

    public class PollerCollection : IDisposable {
        private readonly List<GitPoller> pollers;

        public PollerCollection(params string[] repoLocations) {
            pollers = new List<GitPoller>();
            foreach (var location in repoLocations) {
                Add(location);
            }
        }

        public void Poll(int sleepMs) {
            pollers.ForEach(p => p.Poll(sleepMs));
        }

        public void Add(string repoLocation) {
            pollers.Add(new GitPoller(repoLocation));
        }

        public void Remove(string repoLocation) {
            var poller = pollers.FirstOrDefault(p => p.repositoryLocation == repoLocation);
            poller?.Dispose();
        }

        ~PollerCollection() {
            Dispose(false);
        }

        public void Dispose() {
            Dispose(true);
        }

        private void Dispose(bool b) {
            if (!b) return;
            pollers.ForEach(p => p.Dispose());

        }
    }

    public class GitPoller : IDisposable {
        private readonly Repository repo;
        private readonly object lockObject;
        private bool shouldCancel;

        private Thread pollerThread;
        private DateTime? lastResetTime;

        public readonly string repositoryLocation;

        public GitPoller(string repositoryLocation) {
            this.repositoryLocation = repositoryLocation;
            repo = new Repository(repositoryLocation);
            lockObject = new object();
        }

        ~GitPoller() {
            Dispose(false);
        }

        public void Poll(int sleepms) {
            pollerThread = new Thread(() => {
                while (true) {
                    lock (lockObject) {
                        if (shouldCancel) break;
                    }
                    var fetchsOps = new FetchOptions { Prune = true };
                    Commands.Fetch(repo, "origin", new[] { "+refs/heads/*:refs/remotes/origin/*" }, fetchsOps, null);

                    var master = repo.Branches["master"];
                    var details = master.TrackingDetails;

                    if (!details.BehindBy.HasValue || !(details.BehindBy > 0)) {
                        Thread.Sleep(sleepms);
                        continue;
                    }
                    repo.Reset(ResetMode.Hard, master.TrackedBranch.Tip);
                    lock (lockObject) {
                        lastResetTime = DateTime.Now;
                        Console.WriteLine($"{repositoryLocation} has an update!");
                    }
                }
            });
            pollerThread.Start();
        }

        public DateTime? ConsumeLastReset() {
            lock (lockObject) {
                var retval = lastResetTime;
                lastResetTime = null;
                return retval;
            }
        }

        public void Dispose() {
            Dispose(true);
        }
        private void Dispose(bool shouldDispose) {
            if (!shouldDispose) return;
            if (pollerThread == null) return;

            lock (lockObject) {
                shouldCancel = true;
            }
            pollerThread.Join();
            Console.WriteLine("Properly disposed!");
        }
    }
}
