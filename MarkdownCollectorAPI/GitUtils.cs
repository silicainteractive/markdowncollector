﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibGit2Sharp;

namespace MarkdownCollectorAPI
{
    public class GitUtils
    {
        public Repository GetRepo(string path) => new Repository(path);
    }
}
